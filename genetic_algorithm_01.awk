#
#
# Lista de Funções:
#
#
#	01. nl				: Imprime um "new line" no data stream.
#	02. print_param			: Imprime no data stream todos os parâmetros de configuração do Algoritmo Genético (AG)
#	03. print_data			: Imprime no data stream uma determinada estrutura de dados/array
#	04. read_data_separator		: Lê o separador de dados usado no arquivo de configuração "config_file"
#	05. read_config_file		: Lê o arquivo de configuração "config_file"
#	06. start_target		: Inicializa a palavra-alvo de acordo com o comprimento definido
#	07. zeros			: Inicializa uma estrutura de dados (array) com zeros e a coloca em contexto numérico
#	08. init_pop			: Inicializa a população do AG.
#	09. gauss			: Gera um número pseudo-aleatório gaussiano com média "mu" e desvio-padrão "sigma"
#	10. denormalize			: Desnormaliza o valor dos genes utilizados no cromossomo
#	11. calc_fitness		: Calcula o fitness para uma população de indivíduos
#	12. stochastic_fight		: Implementa o torneio estocástico para o mecanismo de seleção
#	13. crossover			: Implementa o operador evolutivo de crossover (aritmético) entre dois cromossomos
#	14. mutation			: Implementa o operador de mutação (gaussiana)
#	15. find_best			: Encontra o melhor cromossomo e guarda o seu fitness e seus genes em arrays
#	16. print_best			: Imprime no data stream o melhor cromossomo e seu fitness (função auxiliar)
#	17. print_fitness_and_genes	: Imprime no data stream os fitnesses e os genes de uma população de indivíduos
#	18. elitism			: Implementa o elitismo (mantém sempre o melhor indivíduo na população)
#	19. print_result		: Imprime no data stream o resultado desejado (melhor fitness e melhor indivíduo)
#	20. run_my_simulation		: Roda o AG
#
#
# Lista de Variáveis:
#
#
#	01. separator_file	: Variável do tipo string que armazena o nome do arquivo contendo o separador de dados usado no arquivo de configuração
#	02. config_file		: Variável do tipo string que armazena o nome do arquivo de configuração
#	03. target_file		: Variável do tipo string que armazena a palavra-alvo
#	04. data_sep		: Variável do tipo string que armazena o separador de dados usado no arquivo de configuração
#	05. tsize		: Variável do tipo numérica que armazena o tamanho da palavra-alvo
#	06. psize		: Variável do tipo numérica que armazena o tamanho da população
#	07. genes		: Variável do tipo numérica que armazena a quantidade de genes do cromossomo
#	08. lbound		: Variável do tipo numérica que armazena o limite inferior do domínio da função fitness
#	09. ubound		: Variável do tipo numérica que armazena o limite superior do domínio da função fitness
#	10. g			: Variável do tipo numérica que armazena a quantidade de gerações da simulação
#	11. mu			: Variável do tipo numérica que armazena o valor da média da função de densidade gaussiana
#	12. sigma		: Variável do tipo numérica que armazena o valor do desvio-padrão da função de densidade gaussiana
#	13. pc			: Variável do tipo numérica que armazena a probabilidade de crossover
#	14. pm			: Variável do tipo numérica que armazena a probabilidade de mutação
#	15. ts			: Variável do tipo numérica que armazena o tamanho do torneio estocástico
#	16. target		: Array do tipo numérico que armazena a palavra-alvo
#	17. pop1		: Array do tipo numérico que armazena a população de pais
#	18. pop2		: Array do tipo numérico que armazena a população intermediária após o torneio estocástico
#	19. pop3		: Array do tipo numérico que armazena a população intermediária após o crossover
#	20. fit1		: Array do tipo numérico que armazena o fitness da população de pais
#	21. fit2		: Array do tipo numérico que armazena o fitness da população intermediária pop2
#	22. fit3		: Array do tipo numérico que armazena o fitness da população intermediária pop3 (usado apenas em testes)
#	23. bestind		: Array do tipo numérico que armazena o melhor indivíduo de uma dada geração
#	24. bestindex		: Array do tipo numérico que armazena o índice do melhor indivíduo de uma dada geração
#	25. bestfit		: Array do tipo numérico que armazena o fitness do melhor indivíduo de uma dada geração
#	26. set_param		: Array do tipo numérico que armazena que atribui aos parâmetros do AG os seus valores
#
#


BEGIN {
	separator_file		= "separator_file.dat";
	config_file		= "config_file_ga.dat";
	target_file		= "palavra_alvo.dat";
	data_sep		= read_data_separator(separator_file);
	tsize			= "tsize";
	psize			= "psize";
	genes			= "genes";
	lbound			= "lbound";
	ubound			= "ubound";
	g			= "g";
	mu			= "mu";
	sigma			= "sigma";
	pc			= "pc";
	pm			= "pm"
	ts			= "ts";
	target[0]		= "";
	pop1[0]			= "";
	pop2[0]			= "";
	pop3[0]			= "";
	fit1[0]			= "";
	fit2[0]			= "";
	fit3[0]			= "";
	bestind[0]		= "";
	bestfit[0]		= "";
	bestindex[0]		= "";
	set_param[tsize]	= "";

	srand();
	read_config_file(set_param, config_file, data_sep);

	tsize	= set_param[tsize];
	psize	= set_param[psize];
	genes	= tsize;
	lbound	= set_param[lbound];
	ubound	= set_param[ubound];
	g	= set_param[g];
	mu	= set_param[mu];
	sigma	= set_param[sigma];
	pc	= set_param[pc];
	pm	= 1/genes;
	ts	= set_param[ts];

	run_my_simulation();
}

function nl()
{
	printf("\n");
}

function print_param()
{
	print "data_sep = \""data_sep"\"";
	print "tsize    = ", tsize;
	print "psize    = ", psize;
	print "genes    = ", genes;
	print "lbound   = ", lbound;
	print "ubound   = ", ubound;
	print "g        = ", g;
	print "mu       = ", mu;
	print "sigma    = ", sigma;
	print "pc       = ", pc;
	print "pm       = ", pm;
	print "ts       = ", ts;
}

function print_data(data, nl, nc,     i, k)
{
	for(i = 0; i < nl; i++)
	{
		for(k = 0; k < nc; k++)
			printf("%7.3f ", data[i*nc + k]);
		printf("\n");
	}
}

function read_data_separator(separator_file,     line)
{
	while((getline line < separator_file) > 0)
		return line;
}

function read_config_file(set_param, config_file, data_sep,     i, n, line, store_param)
{
	while((getline line < config_file) > 0)
	{
		gsub("[^A-Za-z0-9."data_sep"]*", "", line);
		n = split(line, store_param, data_sep);
		set_param[store_param[1]] = store_param[2];
	}
}

function start_target(target, tsize, target_file,     line, i)
{
	while(((getline line < target_file) > 0) && (i < tsize))
		target[i++] = line;
}

function zeros(data, nl, nc,     i, k)
{
	for(i = 0; i < nl; i++)
		for(k = 0; k < nc; k++)
			data[i*nc + k] = 0 + 0;
}

function init_pop(pop, ind, genes,     i, k)
{
	for(i = 0; i < ind; i++)
		for(k = 0; k < genes; k++)
			pop[i*genes + k] = rand();
}

function gauss(mu, sigma,     sum, n, i, z)
{
	sum = 0;
	n = 12;
	z = 0;

	for(i = 0; i < n; i++)
		sum += rand();
	z = sum - n/2;
	z = mu + sigma*z;
	return (z);
}

function denormalize(norm, lbound, ubound,     i, k, denorm)
{
	denorm = lbound + (ubound - lbound)*norm;
	return (denorm);
}

function calc_fitness(pop, fit, target, ind, genes, lbound, ubound,     i, k, aux1, aux2)
{
	for(i = 0; i < ind; i++)
	{
		fit[i] = 0;
		aux1 = 0;
		for(k = 0; k < genes; k++)
		{
			aux2 = pop[i*genes + k];
			aux1 = target[k] - denormalize(aux2, lbound, ubound);
			fit[i] += -1*(aux1*aux1);
		}
	}
}

function stochastic_fight(pop1, pop2, fit1, fit2, ind, genes, ts,     i, k, z, aux1, bestfit, bestindex)
{
	for(i = 0; i < ind; i++)
	{
		bestfit = -1000000;
		for(z = 0; z < ts; z++)
		{
			aux1 = int(ind*rand());
			if(fit1[aux1] >= bestfit)
			{
				bestfit = fit1[aux1];
				bestindex = aux1;
			}
		}
		fit2[i] = fit1[bestindex];
		for(k = 0; k < genes; k++)
			pop2[i*genes + k] = pop1[bestindex*genes + k];
	}
			
}


function crossover(pop2, pop3, ind, genes, pc,     i, k, c1, c2, g1, g2, g3, g4, alpha)
{
	for(i = 0; i < ind; i += 2)
	{
		c1 = int(ind*rand());
		c2 = int(ind*rand());
		if(rand() < pc)
		{
			for(k = 0; k < genes; k++)
			{
				g1 = pop2[c1*genes + k];
				g2 = pop2[c2*genes + k];
				alpha = rand();
				g3 = (1 - alpha)*g1 + alpha*g2;
				g4 = (1 - alpha)*g2 + alpha*g1;
				pop3[(i + 0)*genes + k] = g3;
				pop3[(i + 1)*genes + k] = g4;
			}
		}
		else
		{
			for(k = 0; k < genes; k++)
			{
				pop3[(i + 0)*genes + k] = pop2[c1*genes + k];
				pop3[(i + 1)*genes + k] = pop2[c2*genes + k];
			}
		}
	}
}

function mutation(pop1, pop3, ind, genes, pm, mu, sigma,     i, k, mut)
{
	for(i = 0; i < ind; i++)
		for(k = 0; k < genes; k++)
			if(rand() < pm)
			{
				mut = -1;
				while(mut < 0 || mut > 1)
					mut = pop3[i*genes + k] + gauss(mu, sigma);
				pop1[i*genes + k] = mut;
			}
			else
				pop1[i*genes + k] = pop3[i*genes + k];
}

function find_best(pop, fit, bestind, bestfit, ind, genes, g,     i, k, fitaux, besti)
{
	fitaux = -1000000;
	for(i = 0; i < ind; i++)
		if(fit[i] >= fitaux)
		{
			bestfit[g] = fit[i];
			bestindex[g] = i;
			fitaux = fit[i];
			besti = i;
		}
	for(k = 0; k < genes; k++)
		bestind[g*genes + k] = pop[besti*genes + k];
}


function print_best(bestind, bestfit, g, genes,     i, k)
{
	printf("%d %10.3f ", g, bestfit[g]);
	for(k = 0; k < genes; k++)
		printf("%10.3f ", bestind[g*genes + k]);
	printf("\n");
}

function print_fitness_and_genes(pop, fit, ind, genes, g,     i, k)
{
	for(i = 0; i < ind; i++)
	{
		printf("%d %10.3f ", g, fit[i]);
		for(k = 0; k < genes; k++)
			printf("%10.3f ", pop[i*genes + k]);
		printf("\n");
	}
}

function elitism(pop, fit, bestfit, bestind, bestindex, ind, genes, g,     i, k, b1, b2)
{
	if(g < 1)
		return;
	if(bestfit[g - 1] > bestfit[g])
	{
		b1 = bestindex[g - 1];
		b2 = bestindex[g];
		for(k = 0; k < genes; k++)
		{
			pop[b2*genes + k] = bestind[(g-1)*genes + k];
			bestind[g*genes + k] = bestind[(g-1)*genes + k];
			bestindex[g] = bestindex[g-1];
		}
		fit[b2] = bestfit[g-1];
		bestfit[g] = bestfit[g-1];
	}
}

function print_result(pop, fit, genes, g, lbound, ubound,     i, k)
{
	printf("%d %10.6f ", g, fit[g]);
	for(i = 0; i < genes; i++)
		printf("%7.3f ", denormalize(pop[g*genes + i], lbound, ubound));
	printf("\n");
}

function run_my_simulation()
{
	zeros(target, 1, tsize);
	zeros(pop1, psize, genes);
	zeros(pop2, psize, genes);
	zeros(pop3, psize, genes);
	zeros(fit1, psize, 1);
	zeros(fit2, psize, 1);
	zeros(fit3, psize, 1);
	zeros(bestfit, g, 1);
	zeros(bestind, g, genes);

	start_target(target, tsize, target_file);
	init_pop(pop1, psize, genes);
	i = 0;

	while(i < g)
	{
		calc_fitness(pop1, fit1, target, psize, genes, lbound, ubound);
		find_best(pop1, fit1, bestind, bestfit, psize, genes, i);
		elitism(pop1, fit1, bestfit, bestind, bestindex, psize, genes, i);
		print_result(bestind, bestfit, genes, i, lbound, ubound);
		stochastic_fight(pop1, pop2, fit1, fit2, psize, genes, ts);
		crossover(pop2, pop3, psize, genes, pc);
		mutation(pop1, pop3, psize, genes, pm, mu, sigma);
		i++;
	}
}
